# Structural Agnostic Model (SAM)

The structural agnostic model (SAM) is a causal discovery method. It leverages conditional independencies and distributional asymmetries to find the underlying causal structure from observational data. The approach relies on a game between different players estimating each variable distribution conditionally to the others as a neural network and an adversary aimed at discriminating the generated data against the original one. SAM's learning criterion combines distribution estimation, sparsity, and acyclicity constraints to enforce the optimization of the graph structure and parameters through stochastic gradient descent. SAM was extensively experimentally validated on synthetic and real data.

## Authors

  * SAM was initially developed by Diviyan Kalainathan and Olivier Goudet.

## Requirements

  * Python 3.9+
  * Pip
  * Conda (optional, but recommended)
  * See [requirements.txt](./requirements.txt) for the others.


```bash
pip install -r requirements.txt
```

### Using Conda

* Execute the following instruction

```bash
conda env create -f environment.yml
conda activate sam
```
Once the conda environment activated, run the following instruction

```bash
pip install .
```

## Citation

```
@article{kalainathan:22,  
  author    = {Kalainathan, Diviyan and Goudet, Olivier and Guyon, Isabelle and Lopez-Paz, David and Sebag, Mich{\`e}le},
  title     = {Structural Agnostic Modeling: Adversarial Learning of Causal Graphs},
  journal   = {Journal of Machine Learning Research},
  volume    = {23},
  number    = {219},
  pages     = {1--62},
  year      = {2022}
}
```

## License

Copyright (c) 2018-2023 the original author or authors.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the MIT License (MIT).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.