#!/usr/bin/env python

from setuptools import setup, find_packages
import sam

setup(
    name='sam',
    version=sam.__version__,
    description='Structural Agnostic Model',
    long_description=open('README.md').read(),
    author=sam.__author__,
    platforms=['any'],
    license='MIT',
    keywords=['causal discovery', 'causal inference'],
    packages=find_packages(),
    install_requires=[
        'scikit-learn',
        'joblib',
        'networkx',
        'tqdm',
        'statsmodels',
        'requests',
        'torch',
        'torchvision',
        'torchaudio'
    ],
)
